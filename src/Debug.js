import Player from './Player'
import Prospect from './Prospect'
import Research from './Research'
import Time from './Time'

class Debug {
  fixedUpdate () {
    /* eslint-disable */
    $("#debug").text(this.debug())
    /* eslint-enable */
  }

  debug () {
    return JSON.stringify(Player) + JSON.stringify(Prospect) + JSON.stringify(Research) + JSON.stringify(Time)
  }
}

export default new Debug()
