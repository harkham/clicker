import Player from './Player'
import {SECOND_BY_DAY} from './constants'

class Time {
  constructor () {
    this.day = 0
    this.month = 1
    this.temp = 0
  }

  update () {
    /* eslint-disable */
    $('#day').text(this.day)
    $('#month').text(this.month)
    /* eslint-eable */
  }
  
  timeUpdate () {
    this.addOneDay()
  }

  addOneDay () {
    if(this.temp == SECOND_BY_DAY)
    {
      this.day++
      if (this.day === 31) {
        Player.pay()
        this.day = 0
        this.month++
      }
      this.temp = 0
    }
    else{
      this.temp++
    }
  }
}

export default new Time()
