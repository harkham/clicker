import Player from './Player'

class Matrix {
  constructor () {
    this.c = document.getElementById('c')
    this.ctx = null
    this.height = null
    this.text = ''
    this.font_size = 10
    this.columns = null
    this.drops = []
    this.timer = 0
  }

  init () {
    this.ctx = this.c.getContext('2d')
    this.c.height = 100
    this.c.width = 1500
    this.text = this.text.split('')
    this.columns = this.c.width / this.font_size
    for (let x = 0; x < this.columns; x++) {
      this.drops[x] = 1
    }
  }

  addLine (number) {
    this.timer += number
    if ( this.timer >= 1) {
      let multiplicator = Player.lineSecond / 40
      for (let i = 0; i < multiplicator; i++) {
        this.draw()
      }
      this.timer = 0
    }
  }

  draw () {
    this.ctx.fillStyle = 'rgba(255, 255, 255, 0.05)'
    this.ctx.fillRect(0, 0, this.c.width, this.c.height)
    this.ctx.fillStyle = '#0F0'
    this.ctx.font = this.font_size + 'px arial'
    for (let i = 0; i < this.drops.length; i++) {
      var text = this.text[Math.floor(Math.random() * this.text.length)]
      this.ctx.fillText(text, i * this.font_size, this.drops[i] * this.font_size)
      if (this.drops[i] * this.font_size > this.c.height && Math.random() > 0.975) {
        this.drops[i] = 0
      }
      this.drops[i]++
    }
  }
}

export default new Matrix()
