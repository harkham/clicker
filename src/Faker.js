export default class Faker {
    constructor () {
        this.firstnames = ['Alysa',
        'Velda',  
        'Marquerite',  
        'Shenna',  
        'Lannie',  
        'Lelia',  
        'Jutta',  
        'Kelle',  
        'Piedad',  
        'Parthenia',  
        'Gennie',  
        'Ashly',  
        'Jo',  
        'Maxie',  
        'Wai',  
        'Danae',  
        'Francine',  
        'Macy',  
        'Theda',  
        'Brinda',  
    ];

        this.lastnames = ['Wilbur',
        'Vance',
        'Jamey',
        'Nestor',
        'Nigel',
        'Lavern',
        'Rodger',
        'Lanny',
        'Shannon',
        'Tommy',
        'Josiah',
        'Haywood',
        'Titus',
        'Stacy',
        'Ivory',
        'Sandy',
        'Sam',
        'Dannie',
        'Jody',
        'Santos',
        ];
    }
  
    getFirstname()
    {
        let index = Math.floor(Math.random() * (this.firstnames.length))
        return this.firstnames[index];
    }

    getLastname()
    {
        let index = Math.floor(Math.random() * (this.firstnames.length));
        return  this.lastnames[index];
    }
  }

  