import { Local } from './Local'
import * as CONST from './constants'
import Research from './Research'
import Prospect from './Prospect'
import Project from './Project'
import Matrix from './Matrix'
import Alert from './Alert'
import Developer from './Developer'
import DevelopersTeam from './DevelopersTeam'

class Player {
  constructor () {
    this.startupName = null
    this.lineNumber = 0
    this.level = 1
    this.employeeLevel = 1
    this.charge = CONST.START_CHARGE
    this.money = CONST.START_MONEY
    this.nbEmployee = 0
    this.developersTeam = new DevelopersTeam();
    this.nbCommercial = 0
    this.price_training_employee = CONST.BASE_PRICE_TRAINING_EMPLOYEE
    this.local = new Local(1, 10);
    this._projectLv = 1
    this.lineSecond = 0
    this.totalLine = 0
    this._oldTotalLine = 0
    this._fixedTime = 0
    this.unlockRecutement = false
    this._unlockRecutementCommercial = false
    this._unlockRecutementCommercialDone = false
    this._unlockPlayerProjectMenu = false
    this._numberProject1 = 0
    this._numberProject2 = 0
    this._numberProject3 = 0
    this.unlockProject1Done = false
    this.unlockProject2Done = false
    this.unlockProject3Done = false
    this.unlockProject2 = false
    this.unlockLocal2 = false
    this.unlockTrainingEmployee = false
    this._workSpeed = CONST.WORK_SPEED_EMPLOYEE
    this._commercialSpeed = CONST.WORK_SPEED_COMMERCIAL
    this._timerCommercialWork = 0
    this._activeDevelopersWork = true
    this._activeCommercialsWork = true
    this.playerProject = null
    this._activeProject = 1
  }

  update () {
  /* eslint-disable */
    $("#lineNumber").text(this.lineNumber.toFixed(0))
    $("#charge").text(this.charge)
    $("#money").text(this.money)
    $('#maxEmployee').text(this.local.maxEmployee)
    $('#localLevel').text(this.local.level)
    $('#lineSecond').text(this.lineSecond)
    $('#employee').text(this.nbEmployee)
    $('#employeeLevel').text(this.employeeLevel)
    $('#priceTrainingEmployee').text(this.price_training_employee)
    $('#numberProject1').text(this._numberProject1)
    $('#numberProject2').text(this._numberProject2)
    $('#numberProject3').text(this._numberProject3)
    $('#nbDeveloper').text(this.developersTeam.developers.length)
    $('#nbCommercial').text(this.nbCommercial)
    $('#inactivityTrainingDeveloper').text(CONST.BASE_TIME_TRAINING_DEVELOPER_IN_DAY)
    $('#inactivityUpgradeLocal').text(CONST.BASE_TIME_UPGRADE_LOCAL_IN_DAY)
    if(this.playerProject != null)
    {
      $('#playerProjectGoal').text(this.playerProject.goalLine)
      $('#playerProjectProgress').text(this.playerProject.progress.toFixed(0))
      $('#projectName').text(this.playerProject.name)
    }
  // $('#playerProject').test(this.playerProject)
  /* eslint-enable */
  }

  fixedUpdate () {
    this.unlock()
    this.developerWork()
    this.commercialWork()
    /* eslint-disable */
    /* eslint-disable */
  }

  timeUpdate () {
    this.lineSecondCalcul()
  }

  lineSecondCalcul () {
    this.lineSecond = Math.round(this.totalLine - this._oldTotalLine)
    this._oldTotalLine = this.totalLine
  }

  developerWork () {
    if (this.developersTeam.developers.length > 0 && this._activeDevelopersWork) {
    this.addLine((this.developersTeam.developers.length * this._workSpeed) * (CONST.FIXEDUPDATE_FRAME_PER_SECOND / 1000))
    }
  }

  commercialWork () {
    if (this.nbCommercial > 0 && this._activeCommercialsWork) {
      let resetValue = (1 / (this.nbCommercial * CONST.WORK_SPEED_COMMERCIAL))
      this._timerCommercialWork += (CONST.FIXEDUPDATE_FRAME_PER_SECOND / 1000)
      if (this._timerCommercialWork >= resetValue) {
        this.prospect()
        this._timerCommercialWork = 0
      }
    }
  }

  addLine (number = 1) {
    this.totalLine += number
    if(this._activeProject == 2) {
      this.playerProject.progress += number
    }
    else {
      this.lineNumber += number
    }
    Matrix.addLine(number)
  }

  addDeveloper () {
    if (this.nbEmployee < this.local.maxEmployee && this.money >= CONST.EMPLOYEE_PRICE) {
      this.money -= CONST.EMPLOYEE_PRICE
      this.charge += parseInt(CONST.EMPLOYEE_PRICE)
      this.developersTeam.addDeveloper(new Developer())
      this.nbEmployee++
    }
  }

  addCommercial () {
    if (this.nbEmployee < this.local.maxEmployee && this.money >= CONST.COMMERCIAL_PRICE) {
      this.money -= CONST.COMMERCIAL_PRICE
      this.charge += parseInt(CONST.COMMERCIAL_PRICE)
      this.nbCommercial++
      this.nbEmployee++
    }
  }

  newProject (name) {
    this.playerProject = new Project(name, CONST.PLAYER_PROJECT_GOAL)
  }

  sellProject (level) {
    if (this._projectLv === level && this._projectLv < 3) {
      this._projectLv++
    }
    switch (level) {
      case 1 :
        let price = CONST.PROJECT_1_PRICE_PROJECT
        let lines = CONST.PROJECT_1_LINE_REQUEST_PROJECT
        if (this.lineNumber >= lines && this._numberProject1 > 0) {
          this.lineNumber -= lines
          this.money += price
          this._numberProject1--
        }
        if (!Player.unlockProject2) {
          Alert.add('Jolie vente ! Les prochains projets trouvés ont une chance d être plus important !')
          Player.unlockProject2 = true
        }
        break
      case 2 :
        price = CONST.PROJECT_2_PRICE_PROJECT
        lines = CONST.PROJECT_2_LINE_REQUEST_PROJECT
        if (this.lineNumber >= lines && this._numberProject2 > 0) {
          this.lineNumber -= lines
          this.money += price
          this._numberProject2--
        }
        if (!Player.unlockProject3) {
          Alert.add('Jolie vente ! Les prochains projets trouvés ont une chance d être plus important !')
          Player.unlockProject3 = true

        }
        break
      case 3 :
        price = CONST.PROJECT_3_PRICE_PROJECT
        lines = CONST.PROJECT_3_LINE_REQUEST_PROJECT
        if (this.lineNumber >= lines && this._numberProject3 > 0) {
          this.lineNumber -= lines
          this.money += price
          this._numberProject3--
        }
        if (!Player.unlockProject2) {
          Player.unlockProject2 = true
        }
        break
    }
    return true
  }

  async upgradeLocal (level, maxEmployee, price) {
    if (this.local.level === level - 1 && this.money >= price) {
      this.money -= price
      this._activeDevelopersWork = false
      await this.startResearch(CONST.BASE_TIME_UPGRADE_LOCAL)
      this._activeDevelopersWork = true
      $('#research').show()
      $('#buyLocal2').hide()
      
      this.local = new Local(level, maxEmployee)
    }
  }

  async upgradeCommercial (level, maxEmployee, price) {
    if (this.local.level === level - 1 && this.money >= price) {
      this.money -= price
      this._activeDevelopersWork = false
      this._activeCommercialsWork = false
      await this.startResearch(CONST.BASE_TIME_UPGRADE_LOCAL)
      this._activeDevelopersWork = true
      this._activeCommercialsWork = true
      this._commercialSpeed *= INCREASE_EFFICIENCY_NEW_COMMERCIAL_LEVEL
      this.price_training_employee *= CONST.MULTIPLICATOR_PRICE_NEW_EMPLOYEE_LEVEL
      $('#research').show()
      this.local = new Local(level, maxEmployee)
    }
  }

  async trainingEmployee () {
    if (!Research._inProgress && this.money >= this.price_training_employee) {
      this.money -= this.price_training_employee
      this._activeDevelopersWork = false
      await this.startResearch(CONST.BASE_TIME_TRAINING_DEVELOPER)
      $('#research').show()
      this._activeDevelopersWork = true
      this._workSpeed *= CONST.INCREASE_EFFICIENCY_NEW_EMPLOYEE_LEVEL
      this.price_training_employee *= CONST.MULTIPLICATOR_PRICE_NEW_EMPLOYEE_LEVEL
      this.employeeLevel++
    }
  }

  startResearch (time) {
    $('#research').hide()
    return new Promise(function (resolve, reject) {
      Research.start(time, resolve, reject)
    })
  }

  pay () {
    if (this.money - this.charge >= 0) {
      this.money -= this.charge
    } else {
      this.gameOver()
    }
  }

  run (project) {
    if(project == 1) 
    {
      this._activeProject = 1
    }
    else if(project == 2)
    {
      this._activeProject = 2
    }
  }

  prospect () {
    if (Prospect.prospect()) {
      if (!this._unlockRecutementCommercial) {
        this._unlockRecutementCommercial = true
      }
      let projectUnlock = Math.floor((Math.random() * this._projectLv) + 1)
      console.log(projectUnlock)
      this.unlockNewProject(projectUnlock)
    }
  }

  unlockNewProject (projectUnlock) {
    switch (projectUnlock) {
      case 1 :
        this._numberProject1++
        this.unlockProject1 = true
        break
      case 2 :
        this._numberProject2++
        this.unlockProject2 = true
        break
      case 3 :
        this._numberProject3++
        this.unlockProject3 = true
        break
    }
  }

  unlock () {
    if (!this.unlockRecutement && this.totalLine > CONST.LINE_NUMBER_UNLOCK_EMPLOYEE) {
      /* eslint-disable */
      $('#recrutement').show()
      Alert.add('Vous pouvez désormais embaucher vos premiers développeur !')
      this.unlockRecutement = true
    }
    if(this._unlockRecutementCommercial && !this._unlockRecutementCommercialDone) {
      $('#engageCommercial').show()
      Alert.add('Vous pouvez désormais embaucher vos premiers commerciaux !')
      this._unlockRecutementCommercialDone = true
    }
    if (this.unlockProject1 && !this.unlockProject1Done) {
      $('#sellProject1').show()
      this.unlockProject1Done = true
    }
    if (this.unlockProject2 && !this.unlockProject2Done) {
      $('#sellProject2').show()
   //   this.unlockLocal2 = false
      this.unlockProject2Done = true
    }
    if (this.unlockProject3  && !this.unlockProject3Done) {
      $('#sellProject3').show()
      this.unlockProject3Done = true
  //    this.unlockLocal2 = false
    }
    if (this.nbEmployee == 10 && !this.unlockLocal2) {
      $('#research').show()
      $('#buyLocal2').show()
      Alert.add('Vous pouvez désormais agrandir votre local ! Bravo ! Attention cependant ... les travaux sont long ... votre personnelles sera inactif pendant tout ce temps  !')
      this.unlockLocal2 = true
    }
    if (this.local.level == 2 && !this._unlockPlayerProjectMenu) {
      $('#playerProjectMenu').show()
      Alert.add('Youhou ! Vous pouvez désormais développer vos propre projet ! Attention cependant, vous ne pouvez pas travailler pour vous et vos client en même temps !')
      this._unlockPlayerProjectMenu = true
    }
    if (this.local.level == 2 && !this.unlockTrainingEmployee) {
      $("#buytrainingEmployee").show()
      this.unlockTrainingEmployee = true
    }
    /* eslint-enable */
  }

  gameOver () {
    window.alert('Bankruptcy Perdu gros noob')
  }
}

export default new Player()
