import { UPDATE_FRAME_PER_SECOND, FIXEDUPDATE_FRAME_PER_SECOND } from './constants'
class Update {
  
  constructor (updateFramePerSecond, fixedUpdateFramePerSecond) {
    this.updateFramePerSecond = updateFramePerSecond
    this.functionsToUpdate = []
    this.functionsToFixedUpdate = []
    this.functionsToTimeUpdate = []
    this._updateInterval = null
    this._fixedUpdateInterval = null
    this._timeUpdateInterval = null
    this._fixeTimeUpdate = null
  }

  update () {
    clearInterval(this._updateInterval)
    let t0 = 0
    this._updateInterval = setInterval(() => {
      let t1 = window.performance.now()
      let deltaTime = ((t1 - t0) / 1000)
      t0 = t1
      this.functionsToUpdate.forEach(func => {
        func.update(parseFloat(deltaTime))
      })
    }, UPDATE_FRAME_PER_SECOND)
  }

  timeUpdate () {
    clearInterval(this._timeUpdateInterval)
    this._timeUpdateInterval = setInterval(() => {
      this.functionsToTimeUpdate.forEach(func => {
        func.timeUpdate()
      })
    }, 1000)
  }

  fixedUpdate () {
    clearInterval(this.fixedUpdateInterval)
    this._fixedUpdateInterval = setInterval(() => {
      this.functionsToFixedUpdate.forEach(func => {
        func.fixedUpdate()
      })
    }, FIXEDUPDATE_FRAME_PER_SECOND)
  }

  addToUpdate (obj) {
    this.functionsToUpdate.push(obj)
    this.update()
  }

  addToTimeUpdate (obj) {
    this.functionsToTimeUpdate.push(obj)
    this.timeUpdate()
  }

  addTofixedUpdate (obj) {
    this.functionsToFixedUpdate.push(obj)
    this.fixedUpdate()
  }
  /* eslint-disable */
      /*
      $("#lineNumber").text(player.lineNumber);
      $("#charge").text(player.charge);
      $("#argent").text(player.argent);
      $("#level").text(player.level);
      $("#month").text(player.month);
      $("#day").text(player.day);
      $("#trainingValue").text(player.trainingValue);
      $("#levelEmployee").text(player.employerLevel);
      $("#maxEmployee").text(player.local.maxEmployee);
      $("#localLevel").text(player.local.level);
      $("#lineSecond").text(player.lineSecond);
      $("#priceTrainingEmployee").text(basePriceTrainingEmployee * player.employerLevel);
      $("#debug").text(JSON.stringify(player));
      */
  /* eslint-enable */
}
export default new Update(UPDATE_FRAME_PER_SECOND, FIXEDUPDATE_FRAME_PER_SECOND)
