
import Faker from './Faker';

export default class Developer {
    constructor (level = 1) {
      this.level = level
      this.firstname = ''
      this.lastname = ''
      this.setName()
    
    }

    setName(){
      let faker = new Faker()
      let firstname = this.firstname = faker.getFirstname()
      let lastname = this.lastName = faker.getLastname()
      this.firstname = firstname
      this.lastName = lastname
    }
  }
