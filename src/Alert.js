import { ALERT_DURATION } from './constants'

class Alert {
  constructor () {
    this.texts = []
    this.onShow = false
  }

  fixedUpdate()
  {
    this.show()
  }

  async show () {
    if(!this.onShow && this.texts.length > 0)
    {
      this.onShow = true
      let textsCopy = this.texts
      this.texts = []
      for(let i = 0; i < textsCopy.length; i++)
      {
        await new function(){
          return new Promise(function (resolve) {
            $("#text-alert").text(textsCopy[i]);
            $("#alert").show();
    
            setTimeout(function() {
              $("#alert").hide();
              console.log(i)
              console.log(textsCopy.length)

              resolve()
            }, ALERT_DURATION);
          })
        }
        if(i == textsCopy.length - 1)
        {
          this.onShow = false
        }
      }
  }
}

  add (text) {
    this.texts.push(text)
    console.log(this.texts.length)
  }
}

export default new Alert()
