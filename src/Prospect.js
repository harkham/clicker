class Prospect {
  constructor () {
    this.chance = 1 // %
    this._target = null
    this._sort = null
    this.newTarget()
  }

  update () {
    /* eslint-disable */
    $("#numberSort").text(this._sort)
    $("#numberToFind").text(this._target)
    /* eslint-enable */
  }
  
  prospect () {
    let number = Math.floor((Math.random() * 100) + 1)
    this._sort = number
    if (number === this._target) {
      this.newTarget()
      return true
    }
    return false
  }

  newTarget () {
    this._target = Math.floor((Math.random() * 100) + 1)
  }
}

export default new Prospect()
