import Update from './Update'
import Time from './Time'
import Player from './Player'
import Research from './Research'
import Prospect from './Prospect'
import Debug from './Debug'
import Matrix from './Matrix'
import Alert from './Alert'
import { PROJECTS } from './constants'
import './vue.vue'

let started
/* eslint-disable */
$('#start').click(function () {
if(!started) {

    let val = $("#startupNameInput").val()
    if(val) {
        
      $("#main_view").show()
      started = true;
    
      Player.startupName = val
      Matrix.text = val
      Matrix.init()
      start();
    }
   
}
})

$("#writeLineButton").click(function () {
  Player.addLine(1);
})

$("#prospect").click(function () {
  Player.prospect()
})

$("#engageDeveloper").click(() => {
  Player.addDeveloper()
})

$("#engageCommercial").click(() => {
  Player.addCommercial()
})

$("#sellProject1").click(() => {
  Player.sellProject(1) 
})

$("#sellProject2").click(() => {
  Player.sellProject(2)
})

$("#sellProject3").click(() => {
  Player.sellProject(3)
})

$("#buyLocal2").click(() => {
    Player.upgradeLocal(2, 50, 20000)
})

$("#buytrainingEmployee").click(() => {
  Player.trainingEmployee()
})

$("#runClientProject").click(() => {
  Player.run(PROJECTS.SERVICE_PROJECT)
})

$("#runPlayerProject").click(() => {
  Player.run(PROJECTS.PLAYER_PROJECT)
})

$("#saveProjectName").click(() => {
  let val = $("#projectNameInput").val()
  if(val) {
    Player.newProject(val)
    $("#bodyPlayerProject").show()
    $("#newProject").hide()
  }

})

$("#goto_project_view").click(() => {
  $("#project_view").show()
  $("#main_view_2").hide()
})


$('#back_from_project_view').click(() => {
  $("#main_view_2").show()
  $("#project_view").hide()
})

$('#nbDeveloper').click(() => {
  console.log('test')
})

/* eslint-enable */
function start () {
  Update.addTofixedUpdate(Alert)
  Update.addToTimeUpdate(Player)
  Update.addToTimeUpdate(Time)
  Update.addToUpdate(Time)
  Update.addToUpdate(Player)
  Update.addTofixedUpdate(Player)
  Update.addToUpdate(Research)
  Update.addToUpdate(Prospect)
  Update.addTofixedUpdate(Debug)
/*
  Alert.add('Bonjour, bienvenu dans ce jeu qui s\'appel ... hum on sait pas trop encore')
  Alert.add('Vous allez pouvoir diriger votre propre studio')
  Alert.add('Cliquez sur ECRIRE UNE LIGNE DE CODE pour commencer votre développement')
  Alert.add('Cliquez sur PROSPECTER pour trouver des projets et pouvoir vivre de votre travail')
  Alert.add('ATTENTION, veuillez à faire en sorte de pouvoir payer vos facture à la fin du mois !')*
  Alert.add('Bonne chance :)')
  */
}

/* eslint-enable */
/* eslint no-use-before-define: 2 */
// Update.addToUpdate(Time)
