class Research {
  constructor () {
    this._duration = null
    this._resolve = null
    this._research_progress = 0
    this._inProgress = false
  }

  update (deltaTime) {
    if (this._inProgress) {
      if (deltaTime < 1) {
        this._research_progress += (deltaTime)
      }
      let progression = (this._research_progress / (this._duration / 100)).toFixed(2)
      /* eslint-disable */
      $('#progressBar').css('width', progression + "%")
      $('#progressBar').attr('aria-valuenow', progression)
      $('#progressBar').text(progression + "% Complete");
      /* eslint-enable */
      if (progression >= 100) {
        this.end()
      }
    }
  }

  start (duration, resolve, reject) {
    if (!this._inProgress) {
      this._duration = duration
      this._resolve = resolve
      /* eslint-disable */
      $('#researchBar').show()
      $('#progressBar').show()
      /* eslint-enable */
      this._inProgress = true
    } else {
      reject()
    }
  }

  end () {
    this._inProgress = false
    this._research_progress = 0
    /* eslint-disable */
    $('#progressBar').css('width', 0 + "%")
    $('#progressBar').attr('aria-valuenow', 0)
    $('#researchBar').hide()
    $('#progressBar').hide()
    /* eslint-enable */
    this._resolve()
  }
}

export default new Research()
