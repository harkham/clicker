# 1. Grab the latest node image
FROM node:latest
# 2. Set the working directory inside the container to /app
WORKDIR /app
# 4. Expose port defined in .env file
EXPOSE 3000
# 5. Add package.json to the directory
ADD package*.json /app/
# 6. Install dependencies
RUN npm install

COPY . /app
# 8. Start the app inside the container
CMD ["npm", "run", "watch"]
